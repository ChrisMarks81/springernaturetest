package SpringerNatureTestProject;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

/**
 * Project for testing Springer Nature search functionality.
 *
 */
public class App 
{
    private static String mainPageUrl = "https://link.springer.com/";
    private static String advancedSearchPageUrl = "https://link.springer.com/advanced-search";

    //Set up static variables for XPaths.
    private static String mainPageDefaultXpath = "/html/body/div[4]";
    private static String searchTextEntryBoxXpath = mainPageDefaultXpath + "/div[1]/div[1]/form/div[1]/input";
    private static String searchButtonXpath = mainPageDefaultXpath + "/div[1]/div[1]/form/input";
    private static String searchOptionsButtonXpath = mainPageDefaultXpath + "/div[1]/div[1]/form/div[2]/button";
    private static String advancedSearchListItemXpath = mainPageDefaultXpath
            + "/div[1]/div[1]/form/div[2]/div/ul/li[1]/a";
    private static String browseByDisciplineLawXpath = mainPageDefaultXpath
            + "/div[2]/div/div[2]/div[1]/div/ol/li[12]/a";
    private static String browseByDisciplineMaterialsXpath = mainPageDefaultXpath
            + "/div[2]/div/div[2]/div[1]/div/ol/li[15]/a";

    private static String advancedSearchPageDefaultXpath = mainPageDefaultXpath + "/div[2]/div/div/form/div[1]";
    private static String advancedSearchIncludeWordsXpath = advancedSearchPageDefaultXpath + "/div[3]/input";
    private static String advancedSearchExcludeWordsXpath = advancedSearchPageDefaultXpath + "/div[4]/input";
    private static String advancedSearchFromDateXpath = advancedSearchPageDefaultXpath
            + "/div[7]/div[1]/div/span[1]/input";
    private static String advancedSearchToDateXpath = advancedSearchPageDefaultXpath
            + "/div[7]/div[1]/div/span[3]/input";
    private static String advancedSearchSearchButtonXpath = advancedSearchPageDefaultXpath + "";

    private static String numberOfResultsReturnedXPath = mainPageDefaultXpath + "/div[2]/div[3]/div[1]/div[1]/h1";

    public static void main()
    {
        testSubCategorySearchValidCaseReturnsMultipleResults();
        testAdvancedSearchErrorCaseStartDateAfterEndDate();
    }

    private static void testSubCategorySearchValidCaseReturnsMultipleResults() {
        //Launch browser and navigate to the url.
        FirefoxDriver firefoxDriver = new FirefoxDriver();
        firefoxDriver.navigate().to(mainPageUrl);

        //Select the sub category Materials Science
        firefoxDriver.findElementByXPath(browseByDisciplineMaterialsXpath).click();

        //Enter the search string Nicalon and submit the search
        firefoxDriver.findElementByXPath(searchTextEntryBoxXpath).sendKeys("Nicalon");
        firefoxDriver.findElementByXPath(searchButtonXpath).click();

        //Check the url has changed, can't necessarily predetermine what the URL will be each time.
        assertNotEquals("URL has not changed from " + mainPageUrl, mainPageUrl, firefoxDriver.getCurrentUrl());

        String resultsString = firefoxDriver.findElementByXPath(numberOfResultsReturnedXPath).getText();
        //Check the results string contains the text "";
        assertTrue("Results string did not contain expected text (), was: " + resultsString,
                resultsString.contains("Result(s) for 'Nicalon'"));

        //Check the results string contains more results than 0.
        assertFalse("Zero results returned, more than zero expected",
                resultsString.startsWith("0 Result(s) for "));

        //Close down the browser instance to prevent one test potentially influencing the next.
        firefoxDriver.quit();
    }

    private static void testAdvancedSearchErrorCaseStartDateAfterEndDate() {
        //Launch browser and navigate to the url.
        FirefoxDriver firefoxDriver = new FirefoxDriver();
        firefoxDriver.navigate().to(mainPageUrl);

        //Go to advanced search
        firefoxDriver.findElementByXPath(searchOptionsButtonXpath).click();
        firefoxDriver.findElementByXPath(advancedSearchListItemXpath).click();

        //Set the from date later than the to date.
        firefoxDriver.findElementByXPath(advancedSearchFromDateXpath).sendKeys("2015");
        firefoxDriver.findElementByXPath(advancedSearchToDateXpath).sendKeys("2010");

        //Set the include words field to the search text ("Nicalon").
        firefoxDriver.findElementByXPath(advancedSearchIncludeWordsXpath).sendKeys("Nicalon");

        //Submit the search
        firefoxDriver.findElementByXPath(advancedSearchSearchButtonXpath).click();

        //Check no results were returned.
        assertTrue(firefoxDriver.findElementByXPath(numberOfResultsReturnedXPath).getText()
                .contains("0 Result(s) for "));

        //Close down the browser instance to prevent one test potentially influencing the next.
        firefoxDriver.quit();
    }

}
